**Projeto para versões a partir da 3.0.0.**

Versão anteriores vide https://github.com/pietrobiasuz/poi-helper-api

# Uso

* Adicionar a dependencia ao projeto:

```xml
<version.poi>3.17</version.poi>

<dependency>
<groupId>org.apache.poi</groupId>
<artifactId>poi</artifactId>
<version>3.0.0</version>
</dependency>
```

* Workbook é um conjunto de planilhas. Será fisicamente ao final o arquivo xlsx gerado
* Cada planilha (sheet) é uma aba do workbook. Na API, cada sheet é representada por um SheetDescriptor, que possui o valor e o estilo para todas as células da sheet
* Todos os elementos que um client da poi-helper-api precisa fazer uso estão exclusivamente no package  `org.poihelper.api`. Não é necessário utilizar nenhuma classe da API Apache POI.
* Para o cliente construir os elementos Workbook, Sheet e CellStyle foi definido o pattern Builder.

## Workbook

### Criar

```java
poiHelperWorkbook = PoiHelperWorkbookXSSF.Builder.create().sheets(sheetDescriptor).build();
```

### Para gerar o ByteArray do Workbook (e retornar em um serviço REST, por exemplo)

```java
poiHelperWorkbook.generateByteArray();
```

## Sheet

### Criar SheetDescriptor builder

```java
SheetDescriptor.Builder sheetDescriptorBuilder = SheetDescriptor.Builder.create();
```

### Adicionar valores a sheet

```java
// adicionar a proxima linha
sheetDescriptorBuilder.nextRowValues("Valor linha 1, coluna 1",
    "Valor linha 1, coluna 2",
    "Valor linha 1, coluna 3",
    "Valor linha 1, coluna 4");

// adicionar a proxima linha
sheetDescriptorBuilder.nextRowValues("Valor linha 2, coluna 1",
    "Valor linha 2, coluna 2",
    "Valor linha 2, coluna 3",
    "Valor linha 2, coluna 4");

// continuar na última linha utilizada
sheetDescriptorBuilder.continueRowValues("Valor linha 2, coluna 5",
    "Valor linha 2, coluna 6",
    "Valor linha 2, coluna 7",
    "Valor linha 2, coluna 8");
```

* A API vai gerando uma matriz em memória com os valores setados para, posteriormente, avalia-los juntamente com os estilos definidos, e adicioná-los ao output.
* A API permite a adição apenas sequencial de valores, por exemplo, adiciona-se valores as linhas 1, 2, 3, pula-se a linha 4, adiciona valor a linha 5. Não é mais possível retornar a linha 4 ou anteriores para adicionar ou substituir valores. Os valores apenas percorrem a frente. Isso vale para linhas e colunas.
* Todos os números de coluna estão no formato non-zero based (iniciando todos em 1). Não existe linha ou coluna número ZERO.

### Adicionar estilo a uma linha, coluna ou célula

```java
// define formato de moeda brasileira para a coluna 9 e 10, utilizando um estilo built-in em org.poihelper.api.utils.cellstyle.CellStyles
sheetDescriptorBuilder.styleBuilder()
    .column(CellStyles.Currency.BR, 9, 10);

// também define formato de moeda brasileira para a coluna 9 e 10, agora construindo o estilo com o formato com o builder
sheetDescriptorBuilder.styleBuilder().column(CellStyleDescriptor.Builder.create()
    .format(Masks.Currency.BR).build(), 9, 10);

// define estilo negrito nas linhas 1 e 2
sheetDescriptorBuilder.styleBuilder().row(
    CellStyleDescriptor.Builder.create().fontStyleBuilder(
        FontStyleDescriptor.Builder.create().bold())
    .build(), 1, 2);

// tambem define estilo negrito nas linhas 1 e 2, utilizando um estilo de fonte built-in em org.poihelper.api.utils.cellstyle.CellStyles
sheetDescriptorBuilder.styleBuilder().row(CellStyles.Font.BOLD, 1, 2);

// define o formato de data brasileira para a celula da linha 5 e coluna 7
sheetDescriptorBuilder.styleBuilder().cell(
    CellStyles.Date.BR, 
    CellPlace.from(5, 7));

```
* O estilo em cada célula não é incremental. Assim, o último estilo definido em uma célula, ou utilizando o `CellStyleDescriptor.Builder` ou um estilo built-in em `org.poihelper.api.utils.cellstyle.CellStyles`, será o estilo aplicado. Os estilos anteriores para aquela célula serão sobre-escritos
* Utilizando o `CellStyleDescriptor.Builder` é possível definir várias características de um estilo, como tipo de fonte, formato dos dados (data, moeda), alinhamentos, etc
* O cliente da API não precisa se preocupar em não criar estilos repetidos, pois caso, o core durante o processamento da planilha ao encontrar estilos repetidos, já faz automaticamente o reaproveitamentos dos estilos duplicados
* Caso haja mais de um estilo definido para uma célula, a ordem de precedência na aplicação dos estilos (do mais prioritário para o menos): célula, linha e coluna
* Há na API os seguintes builders: SheetDescriptorBuilder, SheetStyleDescriptorBuilder, CellStyleBuilder, FontStyleBuilder

```java
//exemplo com toda a cascata de builders (não precisa utilizar assim, apenas exemplo informativo)
)
FontStyleDescriptor.Builder fontStyleBuilder =
    FontStyleDescriptor.Builder.create().bold();

CellStyleDescriptor.Builder cellStyleBuilder =
    CellStyleDescriptor.Builder.create()
        .fontStyleBuilder(fontStyleBuilder);

SheetStyleDescriptor.Builder columnStyle = SheetStyleDescriptor.Builder.create()
        .column(cellStyleBuilder.build(), 1);

SheetDescriptor.Builder sheetDescriptorBuilder = SheetDescriptor.Builder.create();

sheetDescriptorBuilder.styleBuilder(columnStyle);
```


## Exemplo completo de utilização

```java
SheetDescriptor.Builder sheetDescriptorBuilder = SheetDescriptor.Builder.create();

sheetDescriptorBuilder.styleBuilder()
    // negrito para primeira linha
    .row(CellStyles.Font.BOLD, 1)
    // formato de moeda para a linha 9
    .column(CellStyles.Currency.BR, 9);

sheetDescriptorBuilder.nextRowValues("Almoxarifado",
                "Código",
                "Material",
                "U.M.",
                "Categoria",
                "Grupo Unid. Adm.",
                "Unid. Adm.",
                "Qtde. Consumida",
                "Valor");

dataSource.stream()
    .forEach(consumo -> sheetDescriptorBuilder.nextRowValues(
        consumo.getEstoque().getAlmoxarifado().getNome(),
        consumo.getEstoque().getMaterial().getCodigo().intValue(),
        consumo.getEstoque().getMaterial().getNome(),
        consumo.getEstoque().getMaterial().getUnidadeMedidaEstoque().getSigla(),
        resolverCategoria(materialCategoriaValors, consumo.getEstoque().getMaterial()),
        consumo.getUnidAdm().getGrupoUnidAdm().getNome(),
        consumo.getUnidAdm().getNome(),
        consumo.getQtdeAtendidasRetiradas().subtract(consumo.getQtdeAtendidasDevolvidas()),
        consumo.getValorRetiradas().subtract(consumo.getValorDevolvidas())));

SheetDescriptor sheetDescriptor = builder.build();

byteArrayOutputStream = Optional.of(
        PoiHelperWorkbookXSSF.Builder.create()
            .sheets(sheetDescriptor).build().generateByteArray());
```
