package org.poihelper.core.sheet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Calendar;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CellValueProcessorTest {
    private static final Integer ANY_INTEGER = Integer.valueOf(10);

    @Test
    public void shouldSetCellValueDate() {
        Cell cell = Mockito.mock(Cell.class);
        Date date = new Date();

        CellValueProcessor.assignCellValue(cell, date);

        verify(cell, times(1)).setCellValue(any(Date.class));
    }

    @Test
    public void shouldSetCellValueCalendar() {
        Cell cell = Mockito.mock(Cell.class);
        Calendar value = Calendar.getInstance();

        CellValueProcessor.assignCellValue(cell, value);

        verify(cell, times(1)).setCellValue(any(Calendar.class));
    }

    @Test
    public void shouldSetCellValueInteger() {
        Cell cell = Mockito.mock(Cell.class);

        CellValueProcessor.assignCellValue(cell, ANY_INTEGER);

        verify(cell, times(1)).setCellValue(ANY_INTEGER);
    }

}
