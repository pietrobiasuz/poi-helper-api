package org.poihelper.application;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.poihelper.api.sheet.SheetDescriptor;
import org.poihelper.application.exceptions.WorkbookGenerationException;
import org.poihelper.core.sheet.SheetProcessor;
import org.poihelper.core.workbook.cellstyle.CellStyleBag;

import validation.Collections;

public class GenerateWorkbook {

    private GenerateWorkbook() {}

    public static ByteArrayOutputStream sxssf(List<SheetDescriptor> sheetDescriptors,
            int windowSize) {

        try (Workbook wb = new SXSSFWorkbook(windowSize)) {
            return createByteArray(wb, sheetDescriptors);
        } catch (Exception e) {
            throw new WorkbookGenerationException(e);
        }

    }

    public static ByteArrayOutputStream xssf(List<SheetDescriptor> sheetDescriptors) {

        try (Workbook wb = new XSSFWorkbook()) {
            return createByteArray(wb, sheetDescriptors);
        } catch (Exception e) {
            throw new WorkbookGenerationException(e);
        }

    }

    private static ByteArrayOutputStream createByteArray(Workbook workbook,
            List<SheetDescriptor> sheetDescriptors) {
        Collections.requireNonEmpty(sheetDescriptors);
        Collections.requireNonNullElements(sheetDescriptors);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        CellStyleBag cellStyleBag = new CellStyleBag(workbook);

        sheetDescriptors.stream().forEachOrdered(sheetDescriptor -> {

            // soh eh possivel setar a sheet name nesse momento
            Sheet sheet = workbook.createSheet(sheetDescriptor.coreSheetDescriptor.sheetName);

            SheetProcessor.process(sheet, sheetDescriptor.coreSheetDescriptor, cellStyleBag);
        });

        writeByteArray(workbook, byteArrayOutputStream);

        return byteArrayOutputStream;
    }

    private static void writeByteArray(Workbook workbook,
            ByteArrayOutputStream byteArrayOutputStream) {
        try {
            workbook.write(byteArrayOutputStream);
        } catch (IOException e) {
            throw new WorkbookGenerationException(e);
        }

    }

}
