package org.poihelper.application.exceptions;

public class WorkbookGenerationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public WorkbookGenerationException(Exception e) {
        super(e);
    }

}
