package org.poihelper.core.sheet;

import java.util.Objects;

import org.poihelper.api.sheet.CellPlace;

/**
 * Identicador das celulas da planilha para o core.
 *
 * <p>Os indices sao zero-based.
 *
 * @author pietro.biasuz
 */
public class CellId {
    private static final int MIN_VALUE = 0;
    private static final int CONVERSION_FROM_PLACE_FACTOR = -1;

    public final int row;
    public final int column;

    private CellId(int rowNum, int columnNum) {
        row = rowNum;
        column = columnNum;
    }

    public static CellId from(int rowNum, int columnNum) {
        validateCreateFrom(rowNum, columnNum);

        return new CellId(rowNum, columnNum);
    }

    public static CellId from(CellPlace cellPlace) {
        int rowNum = convertToId(cellPlace.rowPlace.row);
        int columnNum = convertToId(cellPlace.columnPlace.column);

        validateCreateFrom(rowNum, columnNum);

        return new CellId(rowNum, columnNum);
    }

    private static int convertToId(int placeValue) {
        return placeValue + CONVERSION_FROM_PLACE_FACTOR;
    }

    protected static void validateCreateFrom(Integer row, int column) {
        if (row < MIN_VALUE) {
            throw new IllegalArgumentException("Invalid row position: " + row);
        }

        if (column < MIN_VALUE) {
            throw new IllegalArgumentException("Invalid column position: " + column);
        }
    }

    @Override
    public String toString() {
        return "CellId [row=" + row + ", column=" + column + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }
        CellId other = (CellId) obj;

        return Objects.equals(column, other.column)
                && Objects.equals(row, other.row);
    }




}
