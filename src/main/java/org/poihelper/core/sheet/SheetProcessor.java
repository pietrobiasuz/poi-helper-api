package org.poihelper.core.sheet;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.poihelper.core.workbook.cellstyle.CellStyleBag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processador central da planilha.
 *
 * @author pietro.biasuz
 */
public class SheetProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(SheetProcessor.class);

    private final Sheet sheet;
    private final Map<CellId, Object> cellValues;
    private final SheetStylesProcessor cellStyleProcessor;

    private SheetProcessor(Sheet sheet,
            CellStyleBag cellStyleBag,
            Map<CellId, Object> cellValues,
            CoreSheetStyleDescriptor coreSheetStyleDescriptor) {
        this.sheet = sheet;
        this.cellValues = cellValues;
        this.cellStyleProcessor = new SheetStylesProcessor(
                cellStyleBag,
                coreSheetStyleDescriptor);
    }

    public static void process(Sheet sheet,
            CoreSheetDescriptor coreSheetDescriptor,
            CellStyleBag cellStyleBag) {
        LOGGER.debug("Begin process...");

        SheetProcessor sheetDescriptorProcessor =
                new SheetProcessor(sheet,
                        cellStyleBag,
                        coreSheetDescriptor.cellValues,
                        coreSheetDescriptor.coreSheetStyleDescriptor);

        sheetDescriptorProcessor.createCells();
        LOGGER.debug("Process done.");
    }

    private void createCells() {

        for (Entry<CellId, Object> entry : cellValues.entrySet()) {
            int rowNumber = entry.getKey().row;

            Row row = sheet.getRow(rowNumber);

            if (row == null) {
                row = sheet.createRow(rowNumber);
            }

            createSheetCell(row, entry.getKey(), entry.getValue());
        }

    }

    private void createSheetCell(Row row, CellId cellId, Object cellValue) {
        Cell cell = row.createCell(cellId.column);

        cellStyleProcessor.assignCellStyle(cell, cellId);

        CellValueProcessor.assignCellValue(cell, cellValue);

        LOGGER.trace("Created cell {} with value {}", cellId, cellValue);
    }

}
