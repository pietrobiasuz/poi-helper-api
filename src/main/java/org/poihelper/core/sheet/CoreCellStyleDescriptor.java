package org.poihelper.core.sheet;

import java.util.Objects;
import java.util.Optional;

import org.poihelper.api.sheet.cellstyle.CellStyleDescriptor.HAlign;
import org.poihelper.api.sheet.cellstyle.CellStyleDescriptor.VAlign;
import org.poihelper.api.sheet.cellstyle.FontStyleDescriptor;

/**
 * POJO do core.
 *
 * @author pietro.biasuz
 */
public class CoreCellStyleDescriptor {

    public final Optional<FontStyleDescriptor> fontStyleDescriptor;
    public final Optional<String> format;
    public final Optional<HAlign> hAlign;
    public final Optional<VAlign> vAlign;

    public CoreCellStyleDescriptor(
            Optional<FontStyleDescriptor> fontStyleDescriptor,
            Optional<String> format,
            Optional<HAlign> hAlign,
            Optional<VAlign> vAlign) {
        this.fontStyleDescriptor = fontStyleDescriptor;
        this.format = format;
        this.hAlign = hAlign;
        this.vAlign = vAlign;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.fontStyleDescriptor, this.format, this.hAlign, this.vAlign);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        CoreCellStyleDescriptor other = (CoreCellStyleDescriptor) obj;
        if (format == null) {
            if (other.format != null) {
                return false;
            }

        } else if (!format.equals(other.format)) {
            return false;
        }

        if (fontStyleDescriptor == null) {
            if (other.fontStyleDescriptor != null) {
                return false;
            }

        } else if (!fontStyleDescriptor.equals(other.fontStyleDescriptor)) {
            return false;
        }

        if (hAlign == null) {
            if (other.hAlign != null) {
                return false;
            }

        } else if (!hAlign.equals(other.hAlign)) {
            return false;
        }

        if (vAlign == null) {
            if (other.vAlign != null) {
                return false;
            }

        } else if (!vAlign.equals(other.vAlign)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "CellStyleDescriptor [fontStyleDescriptor=" + fontStyleDescriptor + ", format="
                + format + ", hAlign=" + hAlign + ", vAlign=" + vAlign + "]";
    }

}
