package org.poihelper.core.sheet;

import java.util.Map;

/**
 * POJO do core.
 *
 * @author pietro.biasuz
 */
public class CoreSheetStyleDescriptor {
    public final Map<Integer, CoreCellStyleDescriptor> columnStyleDescriptors;
    public final Map<Integer, CoreCellStyleDescriptor> rowStyleDescriptors;
    public final Map<CellId, CoreCellStyleDescriptor> cellStyleDescriptors;

    public CoreSheetStyleDescriptor(
            Map<Integer, CoreCellStyleDescriptor> columnStyleDescriptors,
            Map<Integer, CoreCellStyleDescriptor> rowStyleDescriptors,
            Map<CellId, CoreCellStyleDescriptor> cellStyleDescriptors) {
        this.columnStyleDescriptors = columnStyleDescriptors;
        this.rowStyleDescriptors = rowStyleDescriptors;
        this.cellStyleDescriptors = cellStyleDescriptors;
    }

}
