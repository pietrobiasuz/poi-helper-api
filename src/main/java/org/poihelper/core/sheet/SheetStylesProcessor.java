package org.poihelper.core.sheet;

import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.poihelper.core.workbook.cellstyle.CellStyleBag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processador dos cell styles.
 *
 * <p>Utilizado pelo processador da sheet para definir os estilos de cada celula.
 *
 * <p>O estilo de uma celula pode ser definido direto na propria celula, na linha e na coluna.
 *
 * <p>A ordem de prioridade eh, da mais alta para a mais baixa: celula, linha e coluna.
 *
 * @author pietro.biasuz
 *
 */
public class SheetStylesProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(SheetStylesProcessor.class);

    private final CellStyleBag cellStyleBag;
    private final CoreSheetStyleDescriptor coreSheetStyleDescriptor;

    SheetStylesProcessor(CellStyleBag cellStyleBag,
            CoreSheetStyleDescriptor coreSheetStyleDescriptor) {
        this.cellStyleBag = cellStyleBag;
        this.coreSheetStyleDescriptor = coreSheetStyleDescriptor;
    }

    void assignCellStyle(Cell cell, CellId cellId) {
        Optional<CoreCellStyleDescriptor> cellstyleDesc = evalCellstyle(cellId);

        if (cellstyleDesc.isPresent()) {
            CoreCellStyleDescriptor cellStyleDescriptor = cellstyleDesc.get();
            LOGGER.trace("Found cellstyle descriptor for cellId: {}", cellStyleDescriptor);

            CellStyle cellStyle = cellStyleBag.get(cellStyleDescriptor);
            cell.setCellStyle(cellStyle);
        }

    }

    private Optional<CoreCellStyleDescriptor> evalCellstyle(CellId cellId) {
        if (coreSheetStyleDescriptor.cellStyleDescriptors.containsKey(cellId)) {
            return Optional.of(coreSheetStyleDescriptor.cellStyleDescriptors.get(cellId));
        }

        if (coreSheetStyleDescriptor.rowStyleDescriptors.containsKey(cellId.row)) {
            return Optional.of(coreSheetStyleDescriptor.rowStyleDescriptors.get(cellId.row));
        }

        if (coreSheetStyleDescriptor.columnStyleDescriptors.containsKey(cellId.column)) {
            return Optional
                    .of(coreSheetStyleDescriptor.columnStyleDescriptors.get(cellId.column));
        }

        return Optional.empty();
    }

}
