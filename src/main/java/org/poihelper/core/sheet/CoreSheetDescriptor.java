package org.poihelper.core.sheet;

import java.util.Map;

/**
 * POJO do core.
 *
 * @author pietro.biasuz
 */
public class CoreSheetDescriptor {

    public final String sheetName;
    public final Map<CellId, Object> cellValues;
    public final CoreSheetStyleDescriptor coreSheetStyleDescriptor;

    public CoreSheetDescriptor(String sheetName, Map<CellId, Object> cellValues,
            CoreSheetStyleDescriptor coreSheetStyleDescriptor) {
        this.sheetName = sheetName;
        this.cellValues = cellValues;
        this.coreSheetStyleDescriptor = coreSheetStyleDescriptor;
    }

}
