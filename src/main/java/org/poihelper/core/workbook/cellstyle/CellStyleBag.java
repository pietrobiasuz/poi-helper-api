package org.poihelper.core.workbook.cellstyle;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.poihelper.core.sheet.CoreCellStyleDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Armazena todos os estilos jah aplicados a nivel de workbook.
 *
 * <p>Sempre reutiliza estilos iguais jah criados para celulas que foram processadas anteriormente.
 *
 * <p>A igualdade entre os estilos eh determinada pelo {@link CoreCellStyleDescriptor#equals(Object)}
 *
 * <p>Novos estilos sao criados pela {@link CellStyleFactory}.
 *
 * @author pietro.biasuz
 *
 */
public class CellStyleBag {
    private static final Logger LOGGER = LoggerFactory.getLogger(CellStyleBag.class);

    private final Map<CoreCellStyleDescriptor, CellStyle> bag;

    private final CellStyleFactory cellStyleFactory;

    public CellStyleBag(Workbook wb) {
        this.cellStyleFactory = new CellStyleFactory(wb);
        this.bag = new HashMap<>();
    }

    public CellStyle get(CoreCellStyleDescriptor cellStyleDescriptor) {
        Optional<CellStyle> findAny =
            bag.entrySet().parallelStream()
                .filter(entry -> entry.getKey().equals(cellStyleDescriptor))
                .findAny().map(Entry::getValue);

        if (findAny.isEmpty()) {
            LOGGER.trace("NOT found cellstyle in bag for descriptor: {}", cellStyleDescriptor);

            CellStyle cellStyle = createCellStyle(cellStyleDescriptor);

            bag.put(cellStyleDescriptor, cellStyle);

            return cellStyle;
        }

        LOGGER.trace("Found cellstyle in bag for descriptor: {}", cellStyleDescriptor);
        return findAny.get();
    }

    private CellStyle createCellStyle(CoreCellStyleDescriptor cellStyleDescriptor) {
        return cellStyleFactory.create(cellStyleDescriptor);
    }

}
