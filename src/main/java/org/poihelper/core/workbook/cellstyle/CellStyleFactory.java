package org.poihelper.core.workbook.cellstyle;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.poihelper.api.sheet.cellstyle.CellStyleDescriptor.HAlign;
import org.poihelper.api.sheet.cellstyle.CellStyleDescriptor.VAlign;
import org.poihelper.api.sheet.cellstyle.FontStyleDescriptor;
import org.poihelper.core.sheet.CoreCellStyleDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Componente responavel por criar os {@link CellStyle}.
 *
 * <p>Quando um {@link CoreCellStyleDescriptor} nao eh encontrado na CellStyleBag,
 * a Bag requisita a criacao desse estilo para essa classe.
 *
 * @author pietro.biasuz
 */
public class CellStyleFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(CellStyleFactory.class);

    private Workbook wb;

    public CellStyleFactory(Workbook wb) {
        this.wb = wb;
    }

    public CellStyle create(CoreCellStyleDescriptor cellStyleDescriptor) {
        LOGGER.trace("Will create cellstyle for descriptor: {}", cellStyleDescriptor);

        CellStyle cellStyle = wb.createCellStyle();

        cellStyleDescriptor.fontStyleDescriptor.ifPresent(fontStyleDescriptor -> {
            Font font = createFont(fontStyleDescriptor);
            cellStyle.setFont(font);
        });

        cellStyleDescriptor.hAlign.ifPresent(hAlign -> cellStyle.setAlignment(
                getHorizontalAlignment(hAlign)));

        cellStyleDescriptor.vAlign.ifPresent(vAlign -> cellStyle.setVerticalAlignment(
                getVerticalAlignment(vAlign)));

        cellStyleDescriptor.format.ifPresent(dataFormat -> cellStyle.setDataFormat(
                wb.createDataFormat().getFormat(dataFormat)));

        LOGGER.trace("CellStyle created.");

        return cellStyle;
    }

    private Font createFont(FontStyleDescriptor fontStyleDescriptor) {
        Font font = wb.createFont();

        if (fontStyleDescriptor.bold) {
            font.setBold(true);
        }

        if (fontStyleDescriptor.italic) {
            font.setItalic(true);
        }

        if (fontStyleDescriptor.underline) {
            font.setUnderline(Font.U_SINGLE);
        }

        return font;
    }

    private HorizontalAlignment getHorizontalAlignment(HAlign hAlign) {
        switch (hAlign) {
        case CENTER:
            return HorizontalAlignment.CENTER;
        case CENTER_SELECTION:
            return HorizontalAlignment.CENTER_SELECTION;
        case DISTRIBUTED:
            return HorizontalAlignment.DISTRIBUTED;
        case FILL:
            return HorizontalAlignment.FILL;
        case GENERAL:
            return HorizontalAlignment.GENERAL;
        case JUSTIFY:
            return HorizontalAlignment.JUSTIFY;
        case LEFT:
            return HorizontalAlignment.LEFT;
        case RIGHT:
            return HorizontalAlignment.RIGHT;
        default:
            throw new IllegalArgumentException(
                    "HAlign is not a valida Horizontal Alignment: " + hAlign);
        }
    }

    private VerticalAlignment getVerticalAlignment(VAlign vAlign) {
        switch (vAlign) {
        case BOTTOM:
            return VerticalAlignment.BOTTOM;
        case CENTER:
            return VerticalAlignment.CENTER;
        case DISTRIBUTED:
            return VerticalAlignment.DISTRIBUTED;
        case JUSTIFY:
            return VerticalAlignment.JUSTIFY;
        case TOP:
            return VerticalAlignment.TOP;
        default:
            throw new IllegalArgumentException(
                    "VAlign is not a valida Vertical Alignment: " + vAlign);
        }
    }

}
