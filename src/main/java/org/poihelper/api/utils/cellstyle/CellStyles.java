package org.poihelper.api.utils.cellstyle;

import org.poihelper.api.sheet.cellstyle.CellStyleDescriptor;
import org.poihelper.api.sheet.cellstyle.FontStyleDescriptor;

/**
 * Utilitario com os CellStyles mais recorrentes.
 *
 * @author pietro.biasuz
 *
 */
public class CellStyles {

    private CellStyles() {}

    public static class Font {

        private Font() {}

        public static final CellStyleDescriptor BOLD =
                CellStyleDescriptor.Builder.create()
                        .fontStyleBuilder(FontStyleDescriptor.Builder.create().bold()).build();

        public static final CellStyleDescriptor ITALIC =
                CellStyleDescriptor.Builder.create()
                        .fontStyleBuilder(FontStyleDescriptor.Builder.create().italic()).build();

        public static final CellStyleDescriptor UNDERLINE =
                CellStyleDescriptor.Builder.create()
                        .fontStyleBuilder(FontStyleDescriptor.Builder.create().underline()).build();
    }

    public static class Currency {

        private Currency() {}

        public static final CellStyleDescriptor BR =
                CellStyleDescriptor.Builder.create()
                        .format(Masks.Currency.BR)
                        .build();
    }

    public static class Date {

        private Date() {}

        public static final CellStyleDescriptor BR =
                CellStyleDescriptor.Builder.create()
                        .format(Masks.Date.BR)
                        .build();

    }

    public static class DateTime {

        private DateTime() {}

        public static final CellStyleDescriptor BR =
                CellStyleDescriptor.Builder.create()
                        .format(Masks.DateTime.BR)
                        .build();

    }

}
