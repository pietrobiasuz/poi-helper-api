package org.poihelper.api.utils.cellstyle;

import org.poihelper.api.sheet.cellstyle.CellStyleDescriptor;

/**
 * Mascaras para utilizacao com {@link CellStyleDescriptor.Builder#format(String)}.
 *
 * @see org.apache.poi.ss.usermodel.BuiltinFormats
 *
 * @author pietro.biasuz
 */
public class Masks {

    private Masks() {}

    public static class Currency {

        private Currency() {}

        public static final String BR = "R$ #,#0.00";
    }

    public static class Date {

        private Date() {}

        public static final String BR = "dd/mm/yyyy";
        public static final String YYYYMMDD = "yyyy/mm/dd";

    }

    public static class DateTime {

        private DateTime() {}

        public static final String BR = "dd/mm/yyyy hh:mm:ss";
        public static final String YYYYMMDD_HHMMSS = "yyyy/mm/dd hh:mm:ss";

    }

}
