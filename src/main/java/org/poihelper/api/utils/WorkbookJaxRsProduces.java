package org.poihelper.api.utils;

/**
 * Armazena a string para utilizar no {@code Produces} de um controller REST.
 *
 * @author pietro.biasuz
 */
public class WorkbookJaxRsProduces {
    public static final String MS_EXCEL_TYPE = "application/vnd.ms-excel";

    private WorkbookJaxRsProduces() { }
}
