package org.poihelper.api.workbook;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.poihelper.api.sheet.SheetDescriptor;
import org.poihelper.application.GenerateWorkbook;

/**
 * Builder para criar SXSSFWorkbook.
 *
 * <p>Utilizado para planilhas com um grande volume de linhas.
 *
 * @author pietro.biasuz
 */

public class PoiHelperWorkbookSXSSF {

    private List<SheetDescriptor> sheetDescriptors;
    private int windowSize;

    private PoiHelperWorkbookSXSSF(Builder builder) {
        this.sheetDescriptors = builder.sheetDescriptors;
        this.windowSize = builder.windowSize;
    }

    public ByteArrayOutputStream generate() {
        return GenerateWorkbook.sxssf(sheetDescriptors, windowSize);
    }

    public static class Builder {
        private static final int DEFAULT_WINDOW_SIZE = 100;
        private int windowSize;
        private List<SheetDescriptor> sheetDescriptors;

        private Builder() {
            this.sheetDescriptors = new ArrayList<>();
            this.windowSize = DEFAULT_WINDOW_SIZE;
        }

        public static Builder create() {
            return new Builder();
        }

        public Builder windowSize(int windowSize) {
            this.windowSize = windowSize;

            return this;
        }

        public Builder sheets(SheetDescriptor... sheetDescriptors) {
            this.sheetDescriptors = Arrays.asList(sheetDescriptors);

            return this;
        }

        public PoiHelperWorkbookSXSSF build() {
            return new PoiHelperWorkbookSXSSF(this);
        }
    }

}
