package org.poihelper.api.workbook;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.poihelper.api.sheet.SheetDescriptor;
import org.poihelper.application.GenerateWorkbook;

/**
 * Builder para criar XSSFWorkbook.
 *
 * @author pietro.biasuz
 */
public class PoiHelperWorkbookXSSF {

    private List<SheetDescriptor> sheetDescriptors;

    private PoiHelperWorkbookXSSF(Builder builder) {
        this.sheetDescriptors = builder.sheetDescriptors;
    }

    public ByteArrayOutputStream generateByteArray() {
        return GenerateWorkbook.xssf(sheetDescriptors);
    }

    public static class Builder {
        private List<SheetDescriptor> sheetDescriptors;

        private Builder() {
            this.sheetDescriptors = new ArrayList<>();
        }

        public static Builder create() {
            return new Builder();
        }

        public Builder sheets(SheetDescriptor... sheetDescriptors) {
            this.sheetDescriptors = Arrays.asList(sheetDescriptors);

            return this;
        }

        public PoiHelperWorkbookXSSF build() {
            return new PoiHelperWorkbookXSSF(this);
        }
    }

}
