package org.poihelper.api.sheet;

import java.util.Objects;

import org.poihelper.core.sheet.CellId;

/**
 * Posicao de uma celula na planilha.
 *
 * <p>Os valores sao iniciados em 1 (non-zero based).
 *
 * @author pietro.biasuz
 *
 */
public class CellPlace {
    private static final int CONVERSION_FROM_ID_FACTOR = 1;
    public final RowPlace rowPlace;
    public final ColumnPlace columnPlace;

    public CellPlace(RowPlace rowPlace, ColumnPlace columnPlace) {
        this.rowPlace = rowPlace;
        this.columnPlace = columnPlace;
    }

    public static CellPlace from(int row, int column) {
        RowPlace rowPlace = RowPlace.from(row);
        ColumnPlace columnPlace = ColumnPlace.from(column);

        return from(rowPlace, columnPlace);
    }

    public static CellPlace from(CellId cellId) {
        RowPlace rowPlace = RowPlace.from(convertToId(cellId.row));
        ColumnPlace columnPlace = ColumnPlace.from(convertToId(cellId.column));

        return from(rowPlace, columnPlace);
    }

    private static CellPlace from(RowPlace rowPlace, ColumnPlace columnPlace) {
        validateCreateFrom(rowPlace, columnPlace);

        return new CellPlace(rowPlace, columnPlace);
    }

    private static void validateCreateFrom(RowPlace rowPlace, ColumnPlace columnPlace) {
        if (rowPlace == null || columnPlace == null) {
            throw new IllegalArgumentException("Invalid values to create a CellPlace");
        }
    }

    private static int convertToId(int idValue) {
        return idValue + CONVERSION_FROM_ID_FACTOR;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rowPlace, columnPlace);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        CellPlace other = (CellPlace) obj;

        return Objects.equals(columnPlace, other.columnPlace)
                && Objects.equals(rowPlace, other.rowPlace);
    }

    @Override
    public String toString() {
        return "CellPlace [rowPlace=" + rowPlace + ", columnPlace=" + columnPlace + "]";
    }

}
