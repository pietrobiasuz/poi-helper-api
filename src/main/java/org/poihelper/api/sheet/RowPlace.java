package org.poihelper.api.sheet;

import java.util.Objects;

/**
 * Posicao de uma linha na planilha.
 *
 * <p>Os valores sao iniciados em 1 (non-zero based).
 *
 * @author pietro.biasuz
 *
 */
public class RowPlace {
    private static final Integer MIN_VALUE = 1;
    private static final int CONVERSION_TO_ID_FACTOR = -1;

    public final int row;

    private RowPlace(int row) {
        this.row = row;
    }

    public static RowPlace from(int row) {
        validateCreateFrom(row);

        return new RowPlace(row);
    }

    private static void validateCreateFrom(Integer row) {
        if (row < MIN_VALUE) {
            throw new IllegalArgumentException("Invalid row position: " + row);
        }
    }

    public int toZeroBased() {
        return this.row + CONVERSION_TO_ID_FACTOR;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RowPlace other = (RowPlace) obj;
        if (row != other.row) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RowPlace [row=" + row + "]";
    }

}
