package org.poihelper.api.sheet.cellstyle;

import java.util.Objects;

/**
 * Descritor para o estilo da fonte de uma celula.
 *
 * @author pietro.biasuz
 */
public class FontStyleDescriptor {

    public final boolean bold;
    public final boolean italic;
    public final boolean underline;

    private FontStyleDescriptor(Builder builder) {
        this.bold = builder.bold;
        this.italic = builder.italic;
        this.underline = builder.underline;
    }

    @Override
    public String toString() {
        return "FontStyleDescriptor [bold=" + bold + ", italic=" + italic + ", underline="
                + underline + "]";
    }

    public static class Builder {
        private boolean bold;
        private boolean italic;
        private boolean underline;

        private Builder() {
            this.bold = false;
            this.italic = false;
            this.underline = false;
        }

        public static Builder create() {
            return new Builder();
        }

        public Builder bold() {
            this.bold = true;

            return this;
        }

        public Builder italic() {
            this.italic = true;

            return this;
        }

        public Builder underline() {
            this.underline = true;

            return this;
        }

        public FontStyleDescriptor build() {
            return new FontStyleDescriptor(this);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.bold, this.italic, this.underline);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        FontStyleDescriptor other = (FontStyleDescriptor) obj;
        if (bold != other.bold) {
            return false;
        }

        if (italic != other.italic) {
            return false;
        }

        if (underline != other.underline) {
            return false;
        }

        return true;
    }

}
