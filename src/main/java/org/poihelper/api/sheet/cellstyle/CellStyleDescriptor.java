package org.poihelper.api.sheet.cellstyle;

import java.util.Objects;
import java.util.Optional;

import com.google.common.base.Strings;

import org.poihelper.api.utils.cellstyle.Masks;
import org.poihelper.core.sheet.CoreCellStyleDescriptor;

/**
 * Descritor para o estilo de uma celula.
 *
 * @author pietro.biasuz
 */
public class CellStyleDescriptor {

    public final CoreCellStyleDescriptor coreCellStyleDescriptor;

    private CellStyleDescriptor(Builder builder) {
        this.coreCellStyleDescriptor = new CoreCellStyleDescriptor(
                builder.fontStyleDescriptor,
                builder.format,
                builder.hAlign,
                builder.vAlign);
    }

    public static class Builder {
        private Optional<FontStyleDescriptor.Builder> fontStyleDescriptorBuilder;
        private Optional<FontStyleDescriptor> fontStyleDescriptor;
        private Optional<String> format;
        private Optional<HAlign> hAlign;
        private Optional<VAlign> vAlign;

        public Builder() {
            this.fontStyleDescriptorBuilder = Optional.empty();
            this.fontStyleDescriptor = Optional.empty();
            this.format = Optional.empty();
            this.hAlign = Optional.empty();
            this.vAlign = Optional.empty();
        }

        public static Builder create() {
            return new Builder();
        }

        public Builder fontStyleBuilder(FontStyleDescriptor.Builder fontStyleBuilder) {
            if (this.fontStyleDescriptorBuilder.isPresent()) {
                throw new IllegalStateException(
                        "FontStyle builder is already defined");
            }

            Objects.requireNonNull(fontStyleBuilder);
            this.fontStyleDescriptorBuilder = Optional.of(fontStyleBuilder);

            return this;
        }

        public FontStyleDescriptor.Builder fontStyleBuilder() {
            if (this.fontStyleDescriptorBuilder.isEmpty()) {
                fontStyleBuilder(FontStyleDescriptor.Builder.create());
            }

            return this.fontStyleDescriptorBuilder.get();
        }

        /**
         * Seta formato dos dados.
         *
         * @see Masks
         * @see org.apache.poi.ss.usermodel.BuiltinFormats
         *
         * @param format
         * @return
         */
        public Builder format(String format) {
            if (Strings.isNullOrEmpty(format)) {
                throw new IllegalArgumentException("Format must be a non-empty string");
            }

            this.format = Optional.of(format);

            return this;
        }

        public Builder vAlign(VAlign vAlign) {
            this.vAlign = Optional.of(vAlign);

            return this;
        }

        public Builder hAlign(HAlign hAlign) {
            this.hAlign = Optional.of(hAlign);

            return this;
        }

        public CellStyleDescriptor build() {
            if (fontStyleDescriptorBuilder.isPresent()) {
                this.fontStyleDescriptor = Optional.of(fontStyleDescriptorBuilder.get().build());
            }

            return new CellStyleDescriptor(this);
        }

    }

    /**
     * Alinhamento vertical. Veja detalhes na classe
     * {@code org.apache.poi.ss.usermodel.VerticalAlignment }
     *
     * @author pietro.biasuz
     */
    public enum VAlign {
        TOP,
        CENTER,
        BOTTOM,
        JUSTIFY,
        DISTRIBUTED
    }

    /**
     * Alinhamento horizontal. Veja detalhes na classe
     * {@code org.apache.poi.ss.usermodel.HorizontalAlignment }
     *
     * @author pietro.biasuz
     */
    public enum HAlign {
        GENERAL,
        LEFT,
        CENTER,
        RIGHT,
        FILL,
        JUSTIFY,
        CENTER_SELECTION,
        DISTRIBUTED
    }

}
