package org.poihelper.api.sheet;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.collect.Maps;

import org.poihelper.core.sheet.CellId;
import org.poihelper.core.sheet.CoreSheetDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Descriptor central de uma planilha (sheet) de um workbook.
 *
 * @author pietro.biasuz
 */
public class SheetDescriptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(SheetDescriptor.class);

    public final CoreSheetDescriptor coreSheetDescriptor;

    private SheetDescriptor(Builder builder) {
        this.coreSheetDescriptor = new CoreSheetDescriptor(
                builder.sheetName,
                convertToCellId(builder.cellValues),
                builder.sheetStyleDescriptor.coreSheetStyleDescriptor);
    }

    private static Map<CellId, Object> convertToCellId(Map<CellPlace, Object> cellValues) {
        return cellValues.entrySet().parallelStream()
                .collect(Collectors.toMap(entry -> CellId.from(entry.getKey()),
                        Entry::getValue));
    }

    public static class Builder {
        private static final String DEFAULT_SHEET_NAME = "Planilha-1";
        private static final Object EMPTY_VALUE = "";
        private static final int INITIAL_COLUMN = 1;

        private String sheetName;

        // key = row number, value = lista de valores para a celula em sequencia
        private Map<CellPlace, Object> cellValues;

        private SheetStyleDescriptor sheetStyleDescriptor;
        private Optional<SheetStyleDescriptor.Builder> sheetStyleDescriptorBuilder;

        private int column;
        private int row;

        private Builder() {
            this.cellValues = Maps.newHashMap();
            this.column = 1;
            this.row = 0;
            this.sheetName = DEFAULT_SHEET_NAME;
            this.sheetStyleDescriptorBuilder = Optional.empty();
        }

        public static Builder create() {
            return new Builder();
        }

        public Builder sheetName(String sheetName) {
            Objects.requireNonNull(sheetName);

            this.sheetName = sheetName;

            return this;
        }

        private Builder rowValues(Object... cellValuesVarArgs) {
            List<Object> asList = Arrays.asList(cellValuesVarArgs);

            for (Object cellValue : asList) {
                LOGGER.trace("Put on values row={}, column={}: {}", row, column, cellValue);

                if (Objects.nonNull(cellValue)) {
                    cellValues.put(CellPlace.from(row, column), cellValue);
                } else {
                    LOGGER.warn("Trying to put a null value on row={}, column={}, will be ignored",
                            row, column);
                }

                nextColumn();
            }

            return this;
        }

        public Builder nextRowValues(Object... cellValuesP) {
            nextRow();

            return rowValues(cellValuesP);
        }

        public Builder continueRowValues(Object... cellValuesP) {
            return rowValues(cellValuesP);
        }

        private void nextRow() {
            ++row;
            initColumn();
        }

        private void initColumn() {
            this.column = INITIAL_COLUMN;
        }

        private int nextColumn() {
            return ++column;
        }

        /**
         * Adiciona celulas em branco na proxima linha da planilha.
         */
        public Builder nextRowWithBlankCells(int numberOfBlankCells) {
            Object[] array = new String[numberOfBlankCells];
            Arrays.fill(array, EMPTY_VALUE);
            nextRow();

            return rowValues(array);
        }

        /**
         * Adiciona celulas em branco na linha atual da planilha.
         */
        public Builder continueRowWithBlankCells(int numberOfBlankCells) {
            Object[] array = new String[numberOfBlankCells];
            Arrays.fill(array, EMPTY_VALUE);

            return rowValues(array);
        }

        /**
         * Adiciona linhas em branco na planilha
         *
         * @param numberOfLines quantidade de linhas em branco a adicionar
         */
        public Builder nextBlankLines(int numberOfLines) {
            nextRow();
            this.row += numberOfLines;
            initColumn();

            return this;
        }

        public Builder
                styleBuilder(SheetStyleDescriptor.Builder sheetStyleDescriptorBuilder) {
            if (this.sheetStyleDescriptorBuilder.isPresent()) {
                throw new IllegalStateException(
                        "SheetStyleDescriptorBuilder is already defined");
            }

            Objects.requireNonNull(sheetStyleDescriptorBuilder);
            this.sheetStyleDescriptorBuilder = Optional.of(sheetStyleDescriptorBuilder);

            return this;
        }

        public SheetStyleDescriptor.Builder styleBuilder() {
            if (this.sheetStyleDescriptorBuilder.isEmpty()) {
                styleBuilder(SheetStyleDescriptor.Builder.create());
            }

            return this.sheetStyleDescriptorBuilder.get();
        }

        public SheetDescriptor build() {
            SheetStyleDescriptor sheetStyle;
            if (sheetStyleDescriptorBuilder.isPresent()) {
                sheetStyle = sheetStyleDescriptorBuilder.get().build();
            } else {
                sheetStyle = SheetStyleDescriptor.Builder.create().build();
            }

            this.sheetStyleDescriptor = sheetStyle;
            return new SheetDescriptor(this);
        }

    }

}
