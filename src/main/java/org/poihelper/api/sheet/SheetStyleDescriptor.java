package org.poihelper.api.sheet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.CellStyle;
import org.poihelper.api.sheet.cellstyle.CellStyleDescriptor;
import org.poihelper.core.sheet.CellId;
import org.poihelper.core.sheet.CoreCellStyleDescriptor;
import org.poihelper.core.sheet.CoreSheetStyleDescriptor;
import org.poihelper.core.sheet.SheetStylesProcessor;

import validation.Collections;

/**
 * Descritor que armazena todas opcoes que o cliente selecionou de estilos para cada celula,
 * linha e/ou coluna da planilha.
 *
 * <p>O processamento desses estilos ocorre apenas no momento de gerar cada celula.
 *
 * @see SheetStylesProcessor
 *
 * @author pietro.biasuz
 *
 */
public class SheetStyleDescriptor {

    public final CoreSheetStyleDescriptor coreSheetStyleDescriptor;

    private SheetStyleDescriptor(Builder builder) {
        this.coreSheetStyleDescriptor = new CoreSheetStyleDescriptor(
                convertColumnsToInteger(builder.columnStyleDescriptors),
                convertRowsToInteger(builder.rowStyleDescriptors),
                convertCellsToCellId(builder.cellStyleDescriptors));
    }

    // conversoes para o core

    private static Map<Integer, CoreCellStyleDescriptor> convertColumnsToInteger(
            Map<ColumnPlace, CellStyleDescriptor> columnStyleDescriptors) {
        return columnStyleDescriptors.entrySet().parallelStream()
                .collect(Collectors.toMap(entry -> entry.getKey().toZeroBased(),
                        entry -> entry.getValue().coreCellStyleDescriptor));
    }

    private static  Map<Integer, CoreCellStyleDescriptor> convertRowsToInteger(
            Map<RowPlace, CellStyleDescriptor> rowStyleDescriptors) {
        return rowStyleDescriptors.entrySet().parallelStream()
                .collect(Collectors.toMap(entry -> entry.getKey().toZeroBased(),
                        entry -> entry.getValue().coreCellStyleDescriptor));
    }

    private static Map<CellId, CoreCellStyleDescriptor> convertCellsToCellId(
            Map<CellPlace, CellStyleDescriptor> cellStyleDescriptors) {
        return cellStyleDescriptors.entrySet().parallelStream()
                .collect(Collectors.toMap(entry -> CellId.from(entry.getKey()),
                        entry -> entry.getValue().coreCellStyleDescriptor));
    }

    public static class Builder {
        private Map<ColumnPlace, CellStyleDescriptor> columnStyleDescriptors;
        private Map<RowPlace, CellStyleDescriptor> rowStyleDescriptors;
        private Map<CellPlace, CellStyleDescriptor> cellStyleDescriptors;

        private Builder() {
            this.columnStyleDescriptors = new HashMap<>();
            this.rowStyleDescriptors = new HashMap<>();
            this.cellStyleDescriptors = new HashMap<>();
        }

        public static Builder create() {
            return new Builder();
        }

        /**
         * Seta os {@link CellStyle} para as colNums.
         *
         * @param colNums non-zero-based das colunas
         */
        public Builder column(CellStyleDescriptor cellstyleDesc, Integer... colNums) {
            Collections.requireNonEmpty(colNums);

            Arrays.asList(colNums).parallelStream()
                    .forEach(colNum -> {
                        Objects.requireNonNull(cellstyleDesc);

                        columnStyleDescriptors.put(ColumnPlace.from(colNum), cellstyleDesc);
                    });

            return this;
        }

        /**
         * Seta os {@link CellStyle} para as colunas da planilha em ordem.
         *
         * @param cellStyleDescriptors descriptors na ordem das colunas
         */
        public Builder columns(CellStyleDescriptor... cellStyleDescriptors) {
            List<CellStyleDescriptor> cellStyles = Arrays.asList(cellStyleDescriptors);

            for (int ind = 0; ind < cellStyles.size(); ind++) {
                column(cellStyles.get(ind), ind);
            }

            return this;
        }

        /**
         * Seta os {@link CellStyle} para as rowNums.
         *
         * @param rowNums non-zero-based das linhas
         */
        public Builder row(CellStyleDescriptor cellstyleDesc, Integer... rowNums) {
            Collections.requireNonEmpty(rowNums);

            Arrays.asList(rowNums).parallelStream()
                    .forEach(rowNum -> {
                        Objects.requireNonNull(cellstyleDesc);

                        rowStyleDescriptors.put(RowPlace.from(rowNum), cellstyleDesc);
                    });

            return this;
        }

        /**
         * Seta os {@link CellStyle} para as linhas da planilha em ordem.
         *
         * @param cellStyleDescriptors descriptors na ordem das linhas
         */
        public Builder rows(CellStyleDescriptor... cellStyleDescriptors) {
            List<CellStyleDescriptor> cellStyles = Arrays.asList(cellStyleDescriptors);

            for (int ind = 0; ind < cellStyles.size(); ind++) {
                row(cellStyles.get(ind), ind);
            }

            return this;
        }

        /**
         * Seta o {@link CellStyle} para a linha da planilha.
         *
         * @param rowNum non-zero-based da linha
         */
        public Builder cell(CellStyleDescriptor cellstyleDesc, int rowNum, int columNum) {
            CellPlace cellPlace = CellPlace.from(rowNum, columNum);

            return cell(cellstyleDesc, cellPlace);
        }


        /**
         * Seta os {@link CellStyle} para as rowNums.
         *
         * @param rowNums non-zero-based das linhas
         */
        public Builder cell(CellStyleDescriptor cellstyleDesc, CellPlace... cellPlaces) {
            Collections.requireNonEmpty(cellPlaces);

            Arrays.asList(cellPlaces).parallelStream()
                    .forEach(cellPlace -> {
                        Objects.requireNonNull(cellstyleDesc);

                        cellStyleDescriptors.put(cellPlace, cellstyleDesc);
                    });

            return this;
        }

        public SheetStyleDescriptor build() {
            return new SheetStyleDescriptor(this);
        }

    }

}
