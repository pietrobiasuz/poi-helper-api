package org.poihelper.api.sheet;

import java.util.Objects;

/**
 * Posicao de uma coluna na planilha.
 *
 * <p>Os valores sao iniciados em 1 (non-zero based).
 *
 * @author pietro.biasuz
 *
 */
public class ColumnPlace {
    private static final Integer MIN_VALUE = 1;
    private static final int CONVERSION_TO_ID_FACTOR = -1;

    public final int column;

    public ColumnPlace(int column) {
        this.column = column;
    }

    public static ColumnPlace from(int column) {
        validateCreateFrom(column);

        return new ColumnPlace(column);
    }

    private static void validateCreateFrom(Integer row) {
        if (row < MIN_VALUE) {
            throw new IllegalArgumentException("Invalid column position: " + row);
        }
    }

    public int toZeroBased() {
        return this.column + CONVERSION_TO_ID_FACTOR;
    }

    @Override
    public int hashCode() {
        return Objects.hash(column);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ColumnPlace other = (ColumnPlace) obj;
        if (column != other.column) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ColumnPlace [column=" + column + "]";
    }

}
