package validation;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

/**
 * Utilitaria para validacoes sobre collections.
 *
 * @author pietro.biasuz
 *
 */
public class Collections {

    private Collections() {}

    /**
     * Verifica se os varargs nao sao null ou nao possui elementos.
     **/
    @SafeVarargs
    public static <T> void requireNonEmpty(T... objects) {
        requireNonEmpty(Arrays.asList(objects));
    }

    /**
     * Verifica se a collection nao eh null e possui elementos.
     */
    public static void requireNonEmpty(Collection<?> collection) {
        Objects.requireNonNull(collection);

        if (collection.isEmpty()) {
            throw new IllegalArgumentException("Collection is empty");
        }
    }

    /**
     * Verifica se os varargs nao sao null e, se houver elementos,
     * verifica se nao ha elementos null.
     */
    @SafeVarargs
    public static <T> void requireNonNullElements(T... objects) {
        requireNonNullElements(Arrays.asList(objects));
    }

    /**
     * Verifica se a collection nao eh null e, se houver elementos,
     * verifica se nao ha elementos null.
     */
    public static void requireNonNullElements(Collection<?> collection) {
        Objects.requireNonNull(collection);

        for (Object object : collection) {
            Objects.requireNonNull(object, "Null elements is not allowed");
        }
    }

}
